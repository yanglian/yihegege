<?php
namespace app\admin\controller;
use app\admin\model\Physical as PhysicalModel;
class Physical extends Base
{
    public function index()
    {
    	$physical = new PhysicalModel;
    	$list = $physical->field('content',true)->order('id desc')->select();
    	$this->assign('list',$list);
    	return $this->fetch();
    }

    public function edit($id)
    {
        $this->assign('physical',PhysicalModel::get($id));
        return $this->fetch();
    }

    public function update()
    {
        if($this->request->isPost()){
            $post = $this->request->post();
            $physical = new PhysicalModel;
            if($physical->isUpdate(true)->save($post)){
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }
        }
    }

}