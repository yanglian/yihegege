<?php
namespace app\admin\controller;
use app\admin\model\Question as QuestionModel;
class Question extends Base
{
    public function index()
    {
    	$question = new QuestionModel;
    	$list = $question->order('id desc')->paginate(10);
    	$this->assign('list',$list);
    	return $this->fetch();
    }
}