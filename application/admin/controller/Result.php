<?php
namespace app\admin\controller;
use app\admin\model\Result as ResultModel;
class Result extends Base
{
    public function index($openid = '')
    {
    	$result = new ResultModel;
        $where = [];
        if(!empty($openid)){
            $where['r.openid'] = $openid;
        }
    	$list = $result->where($where)->order('id desc')
    	->field('r.*,u.nickname,u.headimgurl')
    	->alias('r')
    	->join('user u','r.openid = u.openid')
    	->paginate(10);
    	$this->assign('list',$list);
    	return $this->fetch();
    }

    public function delete($id)
    {
        if(!ResultModel::destroy($id)){
            $this->error('删除测试记录失败');
        }
        $this->success('删除测试记录成功');
    }

}