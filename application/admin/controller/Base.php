<?php
namespace app\admin\controller;
use think\Controller;
use think\Session;
class Base extends Controller
{
	public function _initialize()
	{
		if($this->request->action() !== 'login'){
			if(!Session::has('admin')){
				$this->redirect('admin/login');
			}
		}
	}
}