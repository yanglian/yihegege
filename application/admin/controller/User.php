<?php
namespace app\admin\controller;
use app\admin\model\User as UserModel;
class User extends Base
{
    public function index($openid = '')
    {
    	$user = new UserModel;
        $where = [];
        if(!empty($openid)){
            $where['openid'] = $openid;
        }
    	$list = $user->where($where)->order('id desc')->paginate(10);
    	$this->assign('list',$list);
    	return $this->fetch();
    }

    //删除会员
    public function delete($id)
    {
        if(!UserModel::destroy($id)){
            $this->error('删除会员失败');
        }
        $this->success('删除会员成功');
    }
}