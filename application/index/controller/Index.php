<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class Index extends Controller
{
    //重复问题
    public $filterQuestion = [64,20,61,62,38,67];
    //重复对应问题
    public $repeatQuestion = [4,5,16,22,23,44];
    //针对性别问题
    public $filterSex = [30,29];
    //反向计分问题
    public $filterBack = [61,62,63,65,66,67];

    public function index()
    {
        $list = Db::name('question')->field('id as tagID,title as tag')->order('id desc')->select();
        foreach ($this->filterQuestion as $item) {
            foreach ($list as $key => $val) {
                //过滤空格
                $list[$key]['tag'] = trim($val['tag']);

                //过滤男女特殊问题
                if($val['tagID'] == $this->filterSex[0]){
                    unset($list[$key]);
                }
                if($val['tagID'] == $this->filterSex[1]){
                    unset($list[$key]);
                }
                //过滤重复问题
                if($item == $val['tagID']){
                    unset($list[$key]);
                }
            }
        }
        rsort($list);
        $list[] = array('tagID' => 2930,'tag'=>'');
        $this->assign('question',json_encode($list));
        return $this->fetch();
    }

    public function analyse()
    {
        error_reporting(E_ALL ^E_WARNING ^E_NOTICE);
    	if($this->request->isPost()){
            $question = $this->request->post()['question'];

            //判断性别
            $sex = '';
            if($question[70] == 1){
                $question[30] = $question[2930];
                $sex = '男';
            }else{
                $sex = '女';
                $question[29] = $question[2930];
            }
            unset($question[70]);
            unset($question[2930]);

            //判断年龄
            $age = '';
            switch ($question[69]) {
                case '1':
                    $age = '28以下';
                    break;
                case '2':
                    $age = '28~35';
                    break;
                case '3':
                    $age = '35~45';
                    break;
                case '4':
                    $age = '45~60';
                    break;
                case '5':
                    $age = '60以上';
                    break;
            }
            unset($question[69]);

            //处理 重复问题 分数一致
            foreach ($this->filterQuestion as $item) {
                foreach ($this->repeatQuestion as $item2) {
                    $question[$item] = $question[$item2];
                }
            }

            //反向计分
            foreach ($this->filterBack as $index) {
                foreach ($question as $key => $val) {
                    if($key == $index){
                        $question[$key] = strval(6 - $val);
                    }
                }
            }
            ksort($question);

            //分组计算
            $list = Db::name('question')->field('id,type')->where('id','not in','69,70')->select();
            $typeCount = Db::name('question')->group('type')->where('id','not in','69,70')->column('type,count(id)');
            $typeCount['湿热质'] -= 1; // 针对男女问题只答一题 所以 -1
            $groupScore = [];
            foreach ($list as $val) {
                switch ($val['type']) {
                    case '痰湿质':
                        $groupScore['痰湿质'] += $question[$val['id']];
                        break;
                    case '血瘀质':
                        $groupScore['血瘀质'] += $question[$val['id']];
                        break;
                    case '湿热质':
                        $groupScore['湿热质'] += $question[$val['id']];
                        break;
                    case '气郁质':
                        $groupScore['气郁质'] += $question[$val['id']];
                        break;
                    case '平和质':
                        $groupScore['平和质'] += $question[$val['id']];
                        break;
                    case '阳虚质':
                        $groupScore['阳虚质'] += $question[$val['id']];
                        break;
                    case '特禀质':
                        $groupScore['特禀质'] += $question[$val['id']];
                        break;
                    case '阴虚质':
                        $groupScore['阴虚质'] += $question[$val['id']];
                        break;
                    case '气虚质':
                        $groupScore['气虚质'] += $question[$val['id']];
                        break;
                }
            }
            ksort($groupScore);

            //计算最终体质
            $data = [];
            foreach ($groupScore as $group => $score) {
                //转化分数＝［（原始分-条目数）/（条目数×4）］×100
                $data[$group] = intval(($score - $typeCount[$group]) / ($typeCount[$group] * 4) * 100);
            }
            $max = $data;
            unset($max['平和质']);

            $physical = '';
            $pos = array_search(max($max),$max);
            if($data['平和质'] >= 60){
                if($data[$pos] < 40){
                    $physical = '平和质';
                }else{
                    $physical = $pos;
                }
            }else{
                $physical = $pos;
            }

            $insert = [];
            $insert['group1'] = $data['痰湿质'];
            $insert['group2'] = $data['血瘀质'];
            $insert['group3'] = $data['湿热质'];
            $insert['group4'] = $data['气郁质'];
            $insert['group5'] = $data['平和质'];
            $insert['group6'] = $data['阳虚质'];
            $insert['group7'] = $data['特禀质'];
            $insert['group8'] = $data['阴虚质'];
            $insert['group9'] = $data['气虚质'];
            $insert['age'] = $age;
            $insert['sex'] = $sex;
            $insert['physical'] = $physical;
            $insert['openid'] = 'c477843ab55fd46ac6dcca3e467bf0fe'; //暂时写死
            $insert['create_time'] = time();
            $insert['update_time'] = time();
            if(Db::name('result')->insert($insert)){
                $physical_id = Db::name('physical')->where('type',$physical)->value('id');
                $this->redirect(url('index/details',['id'=>$physical_id]));
            }else{
                $this->error('操作失败');
            }
    	}
    }

    //测试结果页面
    public function details($id)
    {
        $this->assign('physical',Db::name('physical')->find($id));
        return $this->fetch();
    }
}
