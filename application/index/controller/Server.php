<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Config;
use EasyWeChat\Foundation\Application;
class Server extends Controller
{
	public function index()
	{
		$options = [
		    'debug'  => true,
		    'app_id' => Config::get('appid'),
		    'secret' => Config::get('appsecret'),
		    'token'  => 'easywechat',
		    // 'aes_key' => null, // 可选
		    'log' => [
		        'level' => 'debug',
		        'file'  => RUNTIME_PATH.'easywechat.log', // XXX: 绝对路径！！！！
		    ],
		    //...
		];
		$app = new Application($options);
		$response = $app->server->serve();
		// 将响应输出
		return $response->send();
	}
}